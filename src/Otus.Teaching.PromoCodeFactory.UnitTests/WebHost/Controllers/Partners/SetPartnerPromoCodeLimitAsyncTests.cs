﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private Mock<IRepository<Partner>> _partnersRepositoryMock { get; set; }

        private PartnersController _partnersController { get; set; }

        private Fixture _autoFixture { get; set; }

        private SetPartnerPromoCodeLimitRequest _request { get; set; }

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _partnersRepositoryMock = new Mock<IRepository<Partner>>();

            _partnersController = new PartnersController(_partnersRepositoryMock.Object);

            _autoFixture = new Fixture();


            var behavior = _autoFixture.Behaviors.FirstOrDefault(b => b.GetType() == typeof(ThrowingRecursionBehavior));
            _autoFixture.Behaviors.Remove(behavior);
            _autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());


            _request = new SetPartnerPromoCodeLimitRequest();
        }

        /// <summary>
        /// п.1
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_WhenPartnerNotExist_ReturnNotFound()
        {
            // Arrange

            var partnerId = Guid.NewGuid();

            Partner p = null;

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(p);


            //Act

            var responce = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            //Assert

            responce.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// п.2
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_WhenPartnerIsNotActive_ReturnBadRequest()
        {
            // Arrange

            var partnerId = Guid.Parse("fa294103-b62e-43bb-a9ad-f5098b5d5505");

            Partner p = _autoFixture.Build<Partner>()
                                   .Without(p => p.Id)
                                   .Without(p => p.IsActive)
                                   .Create()
                                   .WithId(partnerId);

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(p);

            //Act

            var responce = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            //Assert

            responce.Should().BeAssignableTo<BadRequestObjectResult>();
            (responce as BadRequestObjectResult).Value.Should().Be("Данный партнер не активен");
        }

        /// <summary>
        /// п.3
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_WhenPartnerHasLimit_ResetPromocodes()
        {
            // Arrange

            var partnerId = Guid.Parse("fa294103-b62e-43bb-a9ad-f5098b5d5505");

            var limit = _autoFixture.Build<PartnerPromoCodeLimit>()
                                .Without(p => p.CancelDate)
                                .Create();

            Partner partner = _autoFixture.Build<Partner>()
                                   .Without(p => p.Id)
                                   .Create()
                                   .WithId(partnerId)
                                   .Active()
                                   .WithLimit(limit);

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act

            var responce = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            //Assert

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        /// <summary>
        /// п.3
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_WhenPartnerHasNoLimit_DoNotResetPromocodes()
        {
            // Arrange

            var partnerId = Guid.Parse("fa294103-b62e-43bb-a9ad-f5098b5d5505");

            var numberIssuedPromoCodes = 777;

            Partner partner = _autoFixture.Build<Partner>()
                                   .Without(p => p.Id)
                                   .Create()
                                   .WithId(partnerId)
                                   .Active()
                                   .WithNumberIssuedPromoCodes(numberIssuedPromoCodes);

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act

            var responce = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            //Assert

            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);
        }

        /// <summary>
        /// п.4
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_WhenSetPartnerPromoCodeLimit_CancelOldLimit()
        {
            // Arrange

            var partnerId = Guid.Parse("fa294103-b62e-43bb-a9ad-f5098b5d5505");

            var limit = _autoFixture.Build<PartnerPromoCodeLimit>()
                                .Without(p => p.CancelDate)
                                .Create();

            Partner partner = _autoFixture.Build<Partner>()
                                   .Without(p => p.Id)
                                   .Create()
                                   .WithId(partnerId)
                                   .Active()
                                   .WithLimit(limit);

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act

            var responce = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            //Assert

            limit.CancelDate.Should().NotBeNull();

        }

        /// <summary>
        /// п.5
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_WhenSetLimitNoMoreThanZero_ReturnBadRequest()
        {
            // Arrange

            _request.Limit = 0;

            var partnerId = Guid.Parse("fa294103-b62e-43bb-a9ad-f5098b5d5505");

            Partner partner = _autoFixture.Build<Partner>()
                                   .Without(p => p.Id)
                                   .Create()
                                   .WithId(partnerId)
                                   .Active();

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act

            var responce = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            //Assert

            responce.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// п.6
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_WhenSetLimit_CallUpdateEntity()
        {
            // Arrange
            
            var partnerId = Guid.Parse("fa294103-b62e-43bb-a9ad-f5098b5d5505");
            _request.Limit = 20;

            Partner partner = _autoFixture.Build<Partner>()
                                   .Without(p => p.Id)
                                   .Create()
                                   .WithId(partnerId)
                                   .Active();

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act

            var responce = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            //Assert
            
            _partnersRepositoryMock.Verify(x=>x.UpdateAsync(It.IsAny<Partner>()), Times.Once());

        }
    }
}