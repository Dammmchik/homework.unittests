﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    internal static class PartnerExtensions
    {
        internal static Partner WithId(this Partner partner, Guid id)
        {
            partner.Id = id;

            return partner;
        }

        internal static Partner WithLimit(this Partner partner, PartnerPromoCodeLimit limit)
        {
            partner.PartnerLimits.Add(limit);

            return partner;
        }

        internal static Partner Active(this Partner partner)
        {
            partner.IsActive = true;

            return partner;
        }

        internal static Partner WithNumberIssuedPromoCodes(this Partner partner, int numberIssuedPromoCodes)
        {
            partner.NumberIssuedPromoCodes = numberIssuedPromoCodes;

            return partner;
        }
    }
}
